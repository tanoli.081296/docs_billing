## **GT Billing**

- **GT Billing** GT Billing là hệ thống cung cấp thông tin game cho các **Shopfront** để tạo đơn hàng cho **End-user** thực hiện thanh toán và tiến hành giao hàng vào game

- **Mục tiêu**

    - Đáp ứng linh hoạt hơn các yêu cầu đa dạng của việc hỗ trợ local payments, tại nhiều khu vực (multi-regions)
    - Hỗ trợ tích hợp với nhiều dạng loại shopfront (store-portal) cho các game/GS
    - Đi đến thống nhất các quy trình vận hành cho hoạt động thanh toán game:
        - Báo cáo giao dịch, sản lượng
        - Tra soát giao dịch

## **Cấu trúc**
![Billing](../files/GTBiiling.drawio#2)

**1. Shopfront system**

- Các store-portal mà users mua vật phẩm/dịch vụ của các game.
- Có thể do các Studio tự phát triển, hoặc
- Là các portal/app của đối tác thu hộ (hợp tác với các Studio), hoặc
- Là trang như Web Shop hiện tại.

**2. Game Service Provider**

- Thể hiện cho vai trò của Game Publisher
- Và/hoặc các game back-end servers

**3. 3rd Payment Gateways**

- Các cổng thanh toán ngoài VNG

**4. GT Billing System**

- **Inventory (kho hàng)**

    - Chứa thông tin "hàng hóa", vật phẩm” của games
    - Chứa thông tin "cấu hình" về giá, tiền tệ thanh toán, khu vực …và cách bán/báo giá

- **Billing Service**

    - Cung cấp dịch vụ cho các front-end system
    - Giúp front-end trình bày danh mục sản phẩm, xây dựng đơn hàng
    - Cung cấp phương tiện thanh toán  tới users thông qua front-end system
    - Cung cấp phương thức retention (favorite list)

- **Payment Fulfillment**

    - Hoàn tất quá trình thanh toán
    - Thông tin tới game back-end server để “giao hàng”
    - Xác nhận receipt.

- **Reporting**

    - Cung cấp các log report phục vụ cho quy trình vận hành thanh toán
    - Cung cấp các log phục vụ quy trình vận hành hệ thống
    - Cung cấp các công cụ tra soát phục vụ hỗ trợ.

- **Payment Gateway**

    - Cung cấp các phương thức thanh toán của mỗi region.
    - Ở VN, hiện là hệ thống MTO-DBG
    - Thuộc phạm vi quản lý của MTO

## **Phân loại Shopfront**

**1. CAT 1**

![Billing](../files/GTBiiling.drawio#3)

- Tích hợp với MTO Billing và sử dụng Payment Gateways do GT Billing cung cấp.
- Tương tự như Web Shop hiện tại.
- Payment form (payment dialog) hiển thị tới end-user gồm 02 sections:
    - S1: Thể hiện danh mục các vật phẩm/tiền in-game cùng với thông tin giá bán
        - Các thông tin này được query từ Inventory.
    - S2: Liệt kê các phương thức thanh toán để user lựa chọn.
- Payment form có thể cung cấp các tương tác động
    - Cho phép user redeem số dư tồn trong ví (in-game wallet)
    - Cho phép thông tin giá bán tại thời điểm mua; Thông tin promotion
- Tài liệu tích hợp
    - [Order Information retrieval](cat1_extapi.md)

**2. CAT 2, 3**

![Billing](../files/GTBiiling.drawio#4)

- **CAT 2**

    - Tích hợp với GT Billing và sử dụng phương thực thanh toán do chính system đó cung cấp.
        - Mobile Banking App, Internet Banking portal, e-wallet systems
        - User thanh toán từ tài khoản (tài chính) của mình có tại front-end system đó.
    - Sử dụng dịch vụ search do Billing Service cung cấp để giúp user xác định game cần thanh toán.
    - Payment form (payment dialog) hiển thị tới end-user gồm 02 sections:
        - S1: Thể hiện danh mục các vật phẩm/đồng tiền ảo cùng với thông tin giá bán
            Các thông tin này được query từ Inventory.
        - S2: Do shopfront system tự dựng content theo phương thức thanh toán của system đó.
    - Payment form có thể cung cấp các tương tác động
        - Cho phép user redeem số dư tồn trong ví (in-game wallet)
        - Cho phép thông tin giá bán tại thời điểm mua; Thông tin promotion
    - Cần có hợp đồng hợp tác cùng các quy trình phối hợp giữa VNG, GT và đối tác ngoài.

- **CAT 3**

    - Tích hợp với MTO Billing và sử dụng phương thực thanh toán do 3rd payment gateway cung cấp qua shopfront  đó.
        - Các portal của các hệ thống thu hộ tại các country.
        - Sử dụng dịch vụ search do Billing Service cung cấp để giúp user xác định game cần thanh toán.
    - Payment form (payment dialog) hiển thị tới end-user gồm 01 sections:
        - S1: Thể hiện danh mục các vật phẩm/đồng tiền ảo cùng với thông tin giá bán
            Các thông tin này được query từ Inventory.
        - S2: Liệt kê các phương thức thanh toán cho users chọn
    - Phần này do shopfront tự xây dựng content.
    - Payment form có thể cung cấp các tương tác động
        - Cho phép user redeem số dư tồn trong ví (in-game wallet)
        - Cho phép thông tin giá bán tại thời điểm mua; Thông tin promotion
    - Cần có hợp đồng hợp tác cùng các quy trình phối hợp giữa MTO và đối tác ngoài.

    - Tài liệu tích hợp: 
        - [IAP Receipt Validation](iap.md)
        - [ZMP IAP Receipt Validation](zmpiap.md)





