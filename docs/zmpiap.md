_|_|
-------|-|
Version| 1.0
Release date| 08-01-2020
Description| Tài liệu mô tả về các thông tin cần tích hợp với hệ thống IAP Receipt & Purchase Account Type Validation

**Change log**

Version|Description|
-|-|


## 1. API verifyReceipt

* Sequence Diagram:

    ![Billing](../files/GTBiiling.drawio#1)

* Url sandbox: https://sandbox-billing.mto.zing.vn/fe/api/zmpiap/verifyReceipt
* Url production: https://billing.mto.zing.vn/fe/api/zmpiap/verifyReceipt
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientKey `required`|String|Key được cung cấp cho từng sản phẩm khi tích hợp
    userID `required`|String|ID của user
    serverID `optional`|String|ID của server. Nếu serverID nullOrEmtpy thì sẽ lấy serverID trong addInfo (JSON)
    roleID `optional`|String|ID của role. Nếu roleID nullOrEmtpy thì sẽ lấy roleID trong addInfo (JSON)
    productID `required`|String|ID của vật phẩm
    appTransID `required`|String|TransactionID được tạo từ game
    addInfo|String|Dữ liệu bổ sung. 
    paymentGatewayID `required`|Integer|`4`: Apple <br/>`5`: Google
    paymentPartnerID `required`|String|`apple`: Apple<br/>`google`: Google 
    pmcID `required`|String|`appstore`: Apple<br/>`playstore`: Google
    providerID `required`|String|`appstore`: Apple<br/>`playstore`: Google
    **amount** `required`|Double|Số tiền user thanh toán / số hiển thị trên store (giá trị này sdk đc trả về từ Store)
    **country** `required`|String|Quốc gia phát hành thanh toán (ISO 3166-1 alpha-2 code), giá trị này sdk đc trả về từ Store
    **currency** `required`|String|Đơn vi tiền tệ của giao dịch thanh toán (ISO 4217), giá trị này sdk đc trả về từ Store)
    **receipt** `required`|String|Receipt của đơn hàng của Apple / Google Store
    **receiptSign**|String|Chữ ký của Google receipt  (`null` khi là Apple receipt) 
    ts `required`|Long|Thời gian hiện tại (Time in millisecond)
    lang `required`|String|Ngôn ngữ (ISO 4217 & UPPER CASE, Ex: VI, EN ...)
    sig `required`|String|Tham khảo ở [Appendix 1](#1-how-to-create-a-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code  
    returnMessage|String|Error description
    data|OrderEntity| Thông tin order

* Return code/message:

    Code|Description
    ----|----------
    1|Thành công
    **<0**|**Xảy ra lỗi** 
    -1|Dữ liệu không hợp lệ
    -2|Dữ liệu không chính xác (sai signature)
    -8|Hệ thống bảo trì
    -70|PAYMENT_PAY_IAP_ERROR
    -71|PAYMENT_PAY_IAP_INVALID_RECEIPT
    -73|PAYMENT_PAY_IAP_EXPIRED_RECEIPT
    -74|PAYMENT_PAY_IAP_INVALID_PURCHASING_ACCOUNT
    -503|Lỗi hệ thống
    ...|...

* OrderEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    orderNumber|String|ID of order
    
* Example request:
    
    * Google Playstore:
    
    ``` bash
    curl "https://sandbox-billing.mto.zing.vn/fe/api/zmpiap/verifyReceipt" \ 
    -X POST \
    -H "Content-type: application/x-www-form-urlencoded" \
    -d 'addInfo=&amount=50&appTransID=1&clientKey=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjIjo5MDM4NiwiYSI6MTAzODYsInMiOjl9.V6nS3GbapaCcCCHipfD2qHX28osUalVLQksPVmCUOyA&country=TH&currency=THB&lang=EN&paymentGatewayID=5&paymentPartnerID=google&pmcID=playstore&productID=com.vng.dzm2th.goinap1&providerID=playstore&receipt=%7B%22orderId%22%3A%22GPA.3394-4821-7701-26375%22%2C%22packageName%22%3A%22th.vng.dzm2tl%22%2C%22productId%22%3A%22com.vng.dzm2th.goinap1%22%2C%22purchaseTime%22%3A1559300357470%2C%22purchaseState%22%3A0%2C%22developerPayload%22%3A%22%7B%5C%22amount%5C%22%3A%5C%2250%5C%22%2C%5C%22amountMicro%5C%22%3A%5C%2269000000%5C%22%2C%5C%22currency%5C%22%3A%5C%22THB%5C%22%2C%5C%22mtoTransID%5C%22%3A%5C%221%5C%22%2C%5C%22orgAmount%5C%22%3A%5C%2269%2C00%C2%A0%E0%B8%BF%5C%22%7D%22%2C%22purchaseToken%22%3A%22cpblgbejcbjfjclbikkklkmh.AO-J1Ow05jEccxzjANmG5Azfi7TVTpPSSS8st7LrIpe21ImwcNnsdCRBbPMf67AkyCGeP3ZtanjP8gFmPoXch7HEdk7e2ZcOgsTr-V_vxniYZFNUctQojHzIYVWc-fFw_3QKirx6R866%22%7D&receiptSign=Ra%2FOPlfH8cTUlfzvUva%2FmgmJYJwzdGyhDioVNx%2BqO%2FbFdzPls6ojRmCgFf%2FB0mhF76zJEOVCWgmVGbEp6ISMQdZgIfeTVsJ9ARbHZM31AxDGJ00PfhyMCXz0oWFiFZJK%2BMML6E3BI5ApEK5CoRv6s%2FgvnFnP%2Boa7OjGYA0fYH3tZSTLnA%2FDX2kuT%2FdDuyFqa2QxSUkDQCVWqbAztpGSjUDqDjJ9EO0OtApcIlq4eSCQibGUeGNAlBSKVQXxAXo9XbxpA%2FMMYIzAXRC%2B83rXoLjKLLD10t8KRzOVkaNyeMB%2Fd3yrV4UPC8QDtV6l5bV2nLkKzRUjwEC74Fjk0Y4pe4w%3D%3D&roleID=72_90001&serverID=90001&ts=1562121128039&userID=1389007571198853120&sig=cf764012c44b85dd4bac56d254797078'
    ```

    ```json
    {"returnCode":1,"returnMessage":"Success","returnMessage_":"SUCCESS","data":{"orderNumber":1413394862220255232,"orderNumberStr":"1413394862220255232","orderNumberEncoded":"05021-25379-33280-08192","orderStatusMessage":"[MB]SUCCESS","orderDisplayMessage":"[MB]SUCCESS","paymentInstructions":null,"redirectUrl":null,"ztoken":null,"qrCode":null,"balance":null,"smsOtpToken":null,"dbgTransID":null,"dbgStatus":null,"postData":null}}
    ```

<div class="pagebreak"></div>

## Appendix 1. Create signature

* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* Hash key is key_store of google store. This key need to use in google receipt verification, empty for apple

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive.

* All leading and trailing whitespace of a string will have to be trimmed.  

* Example:

    ```bash 
    url = "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item1&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&distSrc=pc&timestamp=1517900546676";
    secret_key = "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    raw = "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "game" + "item" + "1" + "6789" + "test" + "11517900546676" + "12345"
    sig = md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcgameitem16789test1151790054667612345") // "6d1ecb0d9e9d118e032f9c8b5f0ad866"
    ```


