_|_|
-------|-|
Version| 1.0
Release date| 11-06-2019
Description| Tài liệu này dành cho các dịch vụ muốn tích hợp với GT Billing để lấy thông tin thanh toán của Shopfront (VD: Web Shop)

**Change log**

Version|Description|
-|-|
1.0|Khởi tạo

## 1. Lấy thông tin đơn hàng

- GT Billing cung cấp 2 giao thức cho việc lấy thông tin đơn hàng:
    - Khi user hoàn thành thanh toán & GT Billing giao hàng thành công, **GT Billing** sẽ notify đến **end-point** đã đăng kí và tích hợp theo đặc tả **[notifyOrderResult](#1-push-order-result)**. Lưu ý: End-point cần kiểm tra notify 

    - Lấy thông tin đơn hàng thông qua API **[getOrderResult](#2-api-get-order-result)**

- GT Billing hỗ trợ 2 môi trường:
    - Sandbox, thanh toán trên [Web Shop Sandbox](https://sandbox-pay.mto.zing.vn) và sử dụng thẻ test
    - Production


### 1. Push Order Result 

- Đăng kí thông tin: Cung cấp URL cho GT Billing theo môi trường cần tích hợp
- Protocol: HTTP POST
- Content-Type: application/x-www-form-urlencoded charset=utf-8
- Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    order|OrderStatusEntity|Thông tin đơn hàng
    ts|Long|Thời gian (milliseconds) 
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

- OrderStatusEntity:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    orderNumber|Long|Mã đơn hàng
    clientID|Integer|clientID
    appID|Integer|appID
    paymentTransID|String| Mã thanh toán
    orderStatus|Integer|Trạng thái đơn hàng
    paymentStatus|String|Trạng thái thanh toán
    createTime|Long|Thời gian khởi tạo đơn hàng
    products|OrderProductEntity[]|Thông tin các vật phẩm
    payingAmount|Double|Số tiền user thanh toán
    paymentGatewayID|Integer|ID Gateway
    paymentPartnerID|String|ID Partner
    paymentMethodID|String|ID Method
    paymentProviderID|String|ID Provider
    paymentGrossAmount|Double|Số tiền user thanh toán
    paymentNetAmount|Double|Số tiền để giao hàng
    paymentCurrency|String|Đơn vị tiền tệ
    userID|String|ID User
    userIP|String|IP User
    userAgent|String|UserAgent
    serverID|String|ID Server
    roleID|String|ID Role

- OrderProductEntity:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    productID|String|ID vật phẩm
    productType|String|Dạng vậy phẩm
    price|Doube|Giá vật phẩm
    currency|String|Đơn vị tiền tệ
    quantity|Integer|Số lượng
    conversionRate (optional)|Float|Tỉ lệ chuyển đổi
    conversionDenom (optional)|HashMap<Double, Double>|Tỉ lệ quy đổi theo mệnh giá

- Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description

<div class="pagebreak"></div>

###2. Get Order Result

- Production Url: https://billing.mto.zing.vn/ext/api/order/getResult
- Sandbox Url: https://sandbox-billing.mto.zing.vn/ext/api/order/getResult
- Protocol: HTTP GET/POST
- Content-Type: application/x-www-form-urlencoded charset=utf-8
- Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
	apiKey|String|API Key
    orderNumber|String| Mã đơn hàng
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|OrderStatusEntity|Thông tin đơn hàng

<div class="pagebreak"></div>

##Appendix 1. Create signature

- Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

- Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

- All parameters except the signature itself of an exchange message will participate to form the signature.

- All parameter values that form the signature must sort alphabetically based on parameter name.

- All parameters that form the signature must in their original format (i.e., not a URL encoded).

- All parameters that form the signature are case sensitive.

- All leading and trailing whitespace of a string will have to be trimmed.  

- Example:

    ```
    URL: "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item1&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&distSrc=pc&timestamp=1517900546676";
    Secret key: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    Raw: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "game" + "item" + "1" + "6789" + "test" + "11517900546676" + "12345"
    Sig: md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcgameitem16789test1151790054667612345") = "6d1ecb0d9e9d118e032f9c8b5f0ad866"
    ```
