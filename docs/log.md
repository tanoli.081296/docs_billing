_|_|
-------|-|
Version| 1.5
Release date| 31-05-2020
Description| Tài liệu mô tả log của GT Billing phục vụ phân tích và hỗ trợ đối soát 

**Change log**

Version|Description|
-|-|
1.0|Khởi tạo|
...|...
1.4|Bổ sung thêm field environment, purchasingType
1.5|Bổ sung thêm field loginType, loginMethod


## **1. Payment transaction log**
- Log này phát sinh khi GT Billing nhận được xác nhận từ Payment Gateway về việc đơn hàng đã được thanh toán thành công, từ đó GT Billing tiến hành giao hàng vào game 
- Format: CSV

    Index|Name|Type|Description|
    -|-|-|-|
    0 | date| String | date
    1 | ts | long | ts
    2 | version| String | version
    3 | orderNumber | long | orderNumber
    4 | acceptanceType | ACCEPTANCE_TYPE | acceptanceType
    5 | appID | int | appID
    6 | userIP | String | userIP
    7 | clientID | int | clientID
    8 | timestamp | long | timestamp
    9 | countryCode | String | countryCode
    10 | currencyCode | String | currencyCode
    11 | lang | String | lang
    12 | userID| String | userID
    13 | serverID | String | serverID
    14 | roleID | String | roleID
    15 | appTransID | String | appTransID
    16 | products | OrderProduct[] | products
    17 | payingAmount| double | payingAmount
    18 | paymentGatewayID | int | paymentGatewayID
    19 | paymentPartnerID | String | paymentPartnerID
    20 | paymentMethodID | String | paymentMethodID
    21 | paymentProviderID | String | paymentProviderID
    22 | embedData | String | embedData
    23 | redirectUrl | String | redirectUrl
    24 | environment | MB_ENVIRONMENT |  DEV("development"), SANDBOX("sandbox"), STAGING("staging"), PRODUCTION("production"), UNKNOWN("unknown");
    25 | loginType | String | loginType
    26 | loginMethod | String | loginMethod
    27 | optional1 | String | optional1
    28 | optional2 | String | optional2 
    29 | optional3 | String | optional3
    30 | optional4 | String | optional4 
    31 | isSuccess | boolean | isSuccess
    32 | paymentTransID | String | paymentTransID
    33 | paymentStatus | String | paymentStatus
    34 | paymentGrossAmount | Double | paymentGrossAmount
    35 | paymentNetAmount | Double | paymentNetAmount
    36 | paymentOrgAmount | Double | paymentOrgAmount
    37 | paymentCurrency | String | paymentCurrency
    38 | paymentCbMethodID | String | paymentCbMethodID
    39 | paymentCbProviderID | String | paymentCbProviderID
    40 | paymentNotiNumber | int | paymentNotiNumber
    41 | receiptNumber | String | receiptNumber
    42 | purchasingType | ORDER_ACCOUNT_PURCHASING_TYPE | UNKNOWN(-1), SANDBOX(0), PRODUCTION(1) <br/> Thông tin về Purchasing Account từ phía Payment provider (AppStore, PlayStore) 
    43 | paymentResponseTime | long | paymentResponseTime

## **2. Delivery transaction log**
- Log này phát sinh khi GT Billing hoàn tất việc giao hàng vào game
- Format: CSV

    Index|Name|Type|Description|
    -|-|-|-|
    0 | date| String | date
    1 | ts | long | ts
    2 | version| String | version
    3 | orderNumber | long | orderNumber
    4 | acceptanceType | ACCEPTANCE_TYPE | PAY_PAYMENT(1), PAY_ACCOUNT_BALANCE(2), PAY_3RDPARTY_PAYMENT(3), PAY_IAP(3),;
    5 | appID | int | appID
    6 | userIP | String | userIP
    7 | clientID | int | clientID
    8 | timestamp | long | timestamp
    9 | countryCode | String | countryCode
    10 | currencyCode | String | currencyCode
    11 | lang | String | lang
    12 | userID| String | userID
    13 | serverID | String | serverID
    14 | roleID | String | roleID
    15 | appTransID | String | appTransID
    16 | products | OrderProduct[] | products
    17 | payingAmount| double | payingAmount
    18 | paymentGatewayID | int | paymentGatewayID
    19 | paymentPartnerID | String | paymentPartnerID
    20 | paymentMethodID | String | paymentMethodID
    21 | paymentProviderID | String | paymentProviderID
    22 | embedData | String | embedData
    23 | redirectUrl | String | redirectUrl
    24 | environment | MB_ENVIRONMENT |  DEV("development"), SANDBOX("sandbox"), STAGING("staging"), PRODUCTION("production"), UNKNOWN("unknown") <br/> Môi trường phát sinh thanh toán của Billing
    25 | loginType | String | loginType
    26 | loginMethod | String | loginMethod
    27 | optional1 | String | optional1
    28 | optional2 | String | optional2 
    29 | optional3 | String | optional3
    30 | optional4 | String | optional4
    31 | isSuccess | boolean | isSuccess
    32 | deliveryTransID | String | deliveryTransID
    33 | deliveryStatus | String | deliveryStatus
    34 | paymentGrossAmount | Double | paymentGrossAmount
    35 | paymentNetAmount | Double | paymentNetAmount
    36 | paymentOrgAmount | Double | paymentOrgAmount
    37 | paymentCurrency | String | paymentCurrency
    38 | paymentCbMethodID | String | paymentCbMethodID
    39 | paymentCbProviderID | String | paymentCbProviderID
    40 | receiptNumber | String | receiptNumber
    41 | purchasingType | ORDER_ACCOUNT_PURCHASING_TYPE | UNKNOWN(-1), SANDBOX(0), PRODUCTION(1) <br/> Thông tin về Purchasing Account từ phía Payment provider (AppStore, PlayStore)

## **3. Object**

**OrderProduct**

Name|Type|Description|
-|-|-|
productID | String | productID
productName | String | productName
productType | MB_SELLINGPRODUCT_PRODUCT_TYPE | CONVERT(1), PACKAGE(2), CONDITIONAL(3), USERWALLET_CONVERT(4), CONVERT_DENOM(5);
price | double | price
currency | String | currency
quantity | int | quantity
conversionRate | float | conversionRate
conversionDenom | Hashmap`<Double, Double>`| conversionDenom


    
 

