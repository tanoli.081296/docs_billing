_|_|
-------|-|
Version| 1.0.4
Release date| 17-09-2020
Description| Tài liệu mô tả về các thông tin cần tích hợp với hệ thống IAP Receipt validation & fulfillment 

**Change log**

Version|Date|Description
:------|:---|:----------
1.0 | 11-06-2019 | Khởi tạo tài liệu với IAP Apple AppStore & Google PlayStore
1.0.1 | 02-06-2020 | - Cập nhật các yêu cầu [Thông tin khởi tạo Google PlayStore](#cung-cap-tu-google-playstore)   <br/> - Bổ sung yêu cầu bắt buộc cho field `serverID` ở [API validateReceipt](#1-api-validatereceipt) <br/>  - Bổ sung IAP Huawei AppGallery
1.0.2 | 25-06-2020 | - Fix lỗi tài liệu pmcID `apple`, `google` thành `appstore` và `playstore` 
1.0.3 | 11-09-2020 | - Bổ sung mô tả hash key cho Huawei AppGallery ở [Appendix 1](#1-how-to-create-a-signature)
1.0.4 | 17-09-2020 | - Bổ sung mô tả về [khuyến nghị gọi lại API](#5-khuyen-nghi-goi-lai-receipt-validation-danh-cho-client) phía client <br/> - Bổ sung thêm param version, clientVersion <br/> - Bổ sung thêm mô tả cho param `ts` theo [Timestamp](https://en.wikipedia.org/wiki/Unix_time)


<div class="pagebreak"></div>

## Client Requirements

### 1. Thông tin khởi tạo

* Cung cấp từ Google PlayStore

    * Thông tin verify receipt

        Name|Description
        -|-
        publicKey | Key được cung cấp từ Google Store, dùng để verify receipt từ google 
        packageName | Package Name được cung cấp từ Google Play Store, dùng kiểm tra packageName trong receipt

    * Thông tin service account, sử dụng để kiểm tra Purchasing Account & retry giao dịch. Tham khảo ở [Google Authorization](https://developers.google.com/android-publisher/authorization)

        Name|Description
        -|-
        client_email | Service account email. Ví dụ: abc@xyz.iam.gserviceaccount.com
        client_private_key |  Service account private key

* Cung cấp từ Apple AppStore

    * Thông tin verify receipt

        Name|Description
        -|-
        bundle_id|Bundle Id được cung cấp từ Apple Store, dùng để kiểm tra bundle_id trong receipt

* Cung cấp từ Huawei AppGallery
    
    * Thông tin verify receipt

        Name|Description
        -|-
        publicKey|Key được cung cấp từ Huawei AppGallery, dùng để verify receipt  
        packageName|PackageName được cung cấp từ Huawei AppGallery, dùng kiểm tra packageName trong receipt
      
    * Thông tin service account, sử dụng để kiểm tra Purchasing Account & retry giao dịch. Tham khảo ở [App-level Access Token Obtaining](https://developer.huawei.com/consumer/en/doc/development/HMS-References/iap-obtain-application-level-AT-v4)

        Name|Description
        -|-
        clientId | client ID
        clientSecret | client Key

<div class="pagebreak"></div>

### 2. Google DeveloperPayload Entity

* Dữ liệu cần được submit khi call GooglePurchase theo tài liệu
 [Attach_Developer_Payload](https://developer.android.com/google/play/billing/billing_library_overview#attach_a_developer_payload) <br/> Dữ liệu này sẽ được sử dụng để kiểm tra & retry giao dịch, tham khảo ở [Appendix 2](#appendix-2-quy-trinh-retry-google-huawei-iap-transaction)
    
    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    userID|String|ID của user
    serverID|String|ID của server
    roleID|String|ID của role
    productID|String|ID của vật phẩm
    appTransID|String|TransactionID được tạo từ game
    addInfo|String|Dữ liệu bổ sung trong trường hợp game backend cần sử dụng
    ts|Long|Thời gian hiện tại ([Timestamp](https://en.wikipedia.org/wiki/Unix_time) in millisecond)

<div class="pagebreak"></div>

### 3. Huawei DeveloperPayload Entity

* Dữ liệu cần được submit khi call createPurchaseIntent theo tài liệu
 [CreatePurchaseIntent](https://developer.huawei.com/consumer/en/doc/development/HMS-References/iap-IapClient-v4#createPurchaseIntent) <br/> Dữ liệu này sẽ được sử dụng để kiểm tra & retry giao dịch, tham khảo ở [Appendix 2](#appendix-2-quy-trinh-retry-google-huawei-iap-transaction)
    
    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    userID|String|ID của user
    serverID|String|ID của server
    roleID|String|ID của role
    productID|String|ID của vật phẩm
    appTransID|String|TransactionID được tạo từ game
    addInfo|String|Dữ liệu bổ sung trong trường hợp game backend cần sử dụng
    ts|Long|Thời gian hiện tại ([Timestamp](https://en.wikipedia.org/wiki/Unix_time) in millisecond)

### 4. Thông tin cấu hình vật phẩm

* Danh sách thông tin vật phẩm cần bán được cấu hình trên tool IAP Catalogue của GT. <br/>Thông tin một vật phẩm bao gồm

    Name|Data Type|Description
    -|-|-
    productID|String|ID của vật phẩm
    productName|String|Tên của vật phẩm
    grossAmount|Double|Số tiền user thanh toán vật phẩm
    netAmount|Double|Số tiền dùng để giao hàng vào Game System 
    currency|String|Đơn vị tiền tệ tương ứng với grossAmount, netAmount 

* Yêu cầu:

    * Các vật phẩm được cấu hình đầy đủ và đồng bộ ở tool vận hành & Google/Apple Store 
    * [Conversion Policy](../docs_policy_process/policy.md#ge-payment-conversion-policy-sep-2018)
     **cần được đảm bảo** bởi các cấu hình về grossAmount, netAmount, currency

<div class="pagebreak"></div>

### 5. Khuyến nghị gọi lại Receipt validation dành cho Client

* Nhằm hạn chế các lỗi về kết nối hoặc hệ thống bảo trì, phía SDK cần gọi lại (retry) [API validateReceipt](#1-api-validatereceipt) khi không nhận được kết quả phản hồi hoặc kết quả lỗi. Tuy nhiên cần có khoảng cách về thời gian giữa mỗi lần gọi để tránh bị khoá do cơ chế chống DDOS của hệ thống 

* Lỗi cần gọi lại (retry) API được hiểu là các mã lỗi `returnCode` <= 0 hoặc không nhận được mã lỗi `returnCode` do lỗi kết nối (vd: timeout, sai format dữ liệu trả về)
 
* Cơ chế khuyến nghị: Khi có phát sinh lỗi (không nhận được kết quả hoặc kết quả lỗi), Client sẽ chủ động gọi lại API đến khi thành công (tối đa 6 lần), và khoảng thời gian giữa mỗi lần tăng dần, tương ứng

    Số lần|Khoảng thời gian giữa 2 lần
    -|-
    0|0
    1|10 giây 
    2|1 phút
    3|15 phút
    4|1 giờ
    5|3 giờ 
    6|12 giờ

<div class="pagebreak"></div>

### 6. List APIs

_|Name|Description
-|----|-----------
1|[API validateReceipt](#1-api-validatereceipt) | - 

#### 1. API validateReceipt

* Sequence Diagram

    ![Billing](../files/GTBiiling.drawio)

* Url development: https://dev-billing.mto.zing.vn/fe/api/iap/payIap
* Url sandbox: https://sandbox-billing.mto.zing.vn/fe/api/iap/payIap
* Url production: https://billing.mto.zing.vn/fe/api/iap/payIap
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded; charset=utf-8
* Input params

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientKey `required`|String|Key được cung cấp cho từng sản phẩm khi tích hợp
    userID `required`|String|ID của user
    serverID `required`|String|ID của server
    roleID `optional`|String|ID của role
    productID `required`|String|ID của vật phẩm
    appTransID `required`|String|TransactionID được tạo từ game
    addInfo `optional`|String|Dữ liệu bổ sung (embeddedData)
    paymentGatewayID `required`|Integer|`4`: Apple <br/>`5`: Google <br/>`6`: Huawei
    paymentPartnerID `required`|String|`apple`: Apple<br/>`google`: Google<br/>`huawei`: Huawei 
    pmcID `required`|String|`appstore`: Apple<br/>`playstore`: Google<br/>`appgallery`: Huawei
    providerID `required`|String|`appstore`: Apple<br/>`playstore`: Google<br/>`appgallery`: Huawei
    **amount** `required`|Double|Số tiền user thanh toán / số hiển thị trên store (giá trị này sdk đc trả về từ Store)
    **country** `required`|String|Quốc gia phát hành thanh toán (ISO 3166-1 alpha-2 code), giá trị này sdk đc trả về từ Store
    **currency** `required`|String|Đơn vi tiền tệ của giao dịch thanh toán (ISO 4217), giá trị này sdk đc trả về từ Store)
    **receipt** `required`|String|Receipt của đơn hàng của Apple / Google / Huawei 
    **receiptSign** `required`|String|Chữ ký Receipt của Google / Huawei  (`null` khi là receipt của Apple) 
    version `optional`| String | version của api. Hiện tại version = `1.0.1`. Param sẽ bắt buộc `required` vào `Jun-2021`   
    clientVersion `optional`| String | version của client. Param sẽ bắt buộc `required` vào `Jun-2021`
    ts `required`|Long|Thời gian gọi API ([Timestamp](https://en.wikipedia.org/wiki/Unix_time) in millisecond)
    lang `required`|String|Ngôn ngữ (ISO 4217 & UPPER CASE, Ex: VI, EN ...)
    sig `required`|String|Tham khảo ở [Appendix 1](#1-how-to-create-a-signature)

* Output params (application/json)

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code  
    returnMessage|String|Error description
    data|OrderEntity| Thông tin order

* Return code/message

    Code|Description
    ----|----------
    1|Thành công
    2|Giao dịch đã được xử lý trước đó (duplicated)
    **<0**|**Xảy ra lỗi** 
    -1|Dữ liệu không hợp lệ
    -2|Dữ liệu không chính xác (sai signature)
    -8|Hệ thống bảo trì
    -503|Lỗi hệ thống
    ...|...

* OrderEntity

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    orderNumber|String|Order transaction
    
* Example request:
    
    * Google Playstore
    
        ```bash
        curl "https://sandbox-billing.mto.zing.vn/fe/api/iap/payIap" \ 
        -X POST \ 
        -H "Content-type: application/x-www-form-urlencoded" \ 
        -d 'addInfo=&amount=50&appTransID=1&clientKey=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjIjo5MDM4NiwiYSI6MTAzODYsInMiOjl9.V6nS3GbapaCcCCHipfD2qHX28osUalVLQksPVmCUOyA&country=TH&currency=THB&lang=EN&paymentGatewayID=5&paymentPartnerID=google&pmcID=playstore&productID=com.vng.dzm2th.goinap1&providerID=playstore&receipt=%7B%22orderId%22%3A%22GPA.3394-4821-7701-26375%22%2C%22packageName%22%3A%22th.vng.dzm2tl%22%2C%22productId%22%3A%22com.vng.dzm2th.goinap1%22%2C%22purchaseTime%22%3A1559300357470%2C%22purchaseState%22%3A0%2C%22developerPayload%22%3A%22%7B%5C%22amount%5C%22%3A%5C%2250%5C%22%2C%5C%22amountMicro%5C%22%3A%5C%2269000000%5C%22%2C%5C%22currency%5C%22%3A%5C%22THB%5C%22%2C%5C%22mtoTransID%5C%22%3A%5C%221%5C%22%2C%5C%22orgAmount%5C%22%3A%5C%2269%2C00%C2%A0%E0%B8%BF%5C%22%7D%22%2C%22purchaseToken%22%3A%22cpblgbejcbjfjclbikkklkmh.AO-J1Ow05jEccxzjANmG5Azfi7TVTpPSSS8st7LrIpe21ImwcNnsdCRBbPMf67AkyCGeP3ZtanjP8gFmPoXch7HEdk7e2ZcOgsTr-V_vxniYZFNUctQojHzIYVWc-fFw_3QKirx6R866%22%7D&receiptSign=Ra%2FOPlfH8cTUlfzvUva%2FmgmJYJwzdGyhDioVNx%2BqO%2FbFdzPls6ojRmCgFf%2FB0mhF76zJEOVCWgmVGbEp6ISMQdZgIfeTVsJ9ARbHZM31AxDGJ00PfhyMCXz0oWFiFZJK%2BMML6E3BI5ApEK5CoRv6s%2FgvnFnP%2Boa7OjGYA0fYH3tZSTLnA%2FDX2kuT%2FdDuyFqa2QxSUkDQCVWqbAztpGSjUDqDjJ9EO0OtApcIlq4eSCQibGUeGNAlBSKVQXxAXo9XbxpA%2FMMYIzAXRC%2B83rXoLjKLLD10t8KRzOVkaNyeMB%2Fd3yrV4UPC8QDtV6l5bV2nLkKzRUjwEC74Fjk0Y4pe4w%3D%3D&roleID=72_90001&serverID=90001&ts=1562121128039&userID=1389007571198853120&sig=cf764012c44b85dd4bac56d254797078'
        ```
        
        ```json
        {"returnCode":1,"returnMessage":"Success","returnMessage_":"SUCCESS","data":{"orderNumber":1413394862220255232,"orderNumberStr":"1413394862220255232","orderNumberEncoded":"05021-25379-33280-08192","orderStatusMessage":"[MB]SUCCESS","orderDisplayMessage":"[MB]SUCCESS","paymentInstructions":null,"redirectUrl":null,"ztoken":null,"qrCode":null,"balance":null,"smsOtpToken":null,"dbgTransID":null,"dbgStatus":null,"postData":null}}
        ```

    * Apple Appstore
    
        ```bash
        curl "https://sandbox-billing.mto.zing.vn/fe/api/iap/payIap" \ 
        -X POST \ 
        -H "Content-type: application/x-www-form-urlencoded" \ 
        -d 'addInfo=&amount=50&appTransID=1&clientKey=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjIjo4MDM4NiwiYSI6MTAzODYsInMiOjh9.ceAH-HVgntPDhxAc92-ZQDCX7ufnSdLkuT-KDK6Ccxg&country=TH&currency=THB&lang=EN&paymentGatewayID=4&paymentPartnerID=apple&pmcID=appstore&productID=com.vng.dzm2th.goinap1&providerID=appstore&receipt=MIIi3wYJKoZIhvcNAQcCoIIi0DCCIswCAQExCzAJBgUrDgMCGgUAMIISgAYJKoZIhvcNAQcBoIIScQSCEm0xghJpMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFrMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBDQIBAQQFAgMB1YgwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjUyMA8CAQMCAQEEBwwFMS4wLjIwFwIBAgIBAQQPDA10aC52bmcuZHptMnRoMBgCAQQCAQIEEEkIYDV7IQYT3ecmyg0zNqEwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAcAgEFAgEBBBQcNuN7RZDNSm%2FGywMNUBt8Qk2fqTAeAgEMAgEBBBYWFDIwMTktMDUtMzFUMTE6MDE6MTNaMB4CARICAQEEFhYUMjAxMy0wOC0wMVQwNzowMDowMFowNgIBBwIBAQQudE6eMlNhuBFFkzTfReAP9raLL2HRglHTgWoja0IWdNorR9nAeTcNRnQOyTuONTBVAgEGAgEBBE1tzE2P5jkG5PApIDBc93zzCT%2FYp1DJODKoGsfvUjqAVVviXvfBmDywS%2B9kVsNTaRHJMs%2FtaRSq%2BbttFN20NN3%2FnzjG1fSXA35vJQ%2FubDCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBATAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTMzMjM1MzQ2MBsCAgapAgEBBBIMEDEwMDAwMDA1MzMyMzUzNDYwHwICBqgCAQEEFhYUMjAxOS0wNS0zMVQxMTowMToxM1owHwICBqoCAQEEFhYUMjAxOS0wNS0zMVQxMTowMToxM1owIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGguZ29pbmFwMTCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI3ODU5NDM1MBsCAgapAgEBBBIMEDEwMDAwMDA1Mjc4NTk0MzUwHwICBqgCAQEEFhYUMjAxOS0wNS0xNVQwODo0MToxOVowHwICBqoCAQEEFhYUMjAxOS0wNS0xNVQwODo0MToxOVowIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGgudGhldHVhbjCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI4OTM4ODcyMBsCAgapAgEBBBIMEDEwMDAwMDA1Mjg5Mzg4NzIwHwICBqgCAQEEFhYUMjAxOS0wNS0xOFQwOTo0NTo0NVowHwICBqoCAQEEFhYUMjAxOS0wNS0xOFQwOTo0NTo0NVowIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGgudGhldHVhbjCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI4OTM5MDk3MBsCAgapAgEBBBIMEDEwMDAwMDA1Mjg5MzkwOTcwHwICBqgCAQEEFhYUMjAxOS0wNS0xOFQwOTo0NzozOVowHwICBqoCAQEEFhYUMjAxOS0wNS0xOFQwOTo0NzozOVowIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGgudGhldHVhbjCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI4OTQ4MDM0MBsCAgapAgEBBBIMEDEwMDAwMDA1Mjg5NDgwMzQwHwICBqgCAQEEFhYUMjAxOS0wNS0xOFQxMTozMTo0OVowHwICBqoCAQEEFhYUMjAxOS0wNS0xOFQxMTozMTo0OVowIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGgudGhldHVhbjCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI5Mzg2NjMyMBsCAgapAgEBBBIMEDEwMDAwMDA1MjkzODY2MzIwHwICBqgCAQEEFhYUMjAxOS0wNS0yMFQxNTo0MTo1NVowHwICBqoCAQEEFhYUMjAxOS0wNS0yMFQxNTo0MTo1NVowIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGgudGhldHVhbjCCAVsCARECAQEEggFRMYIBTTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTMwMjIxMzE3MBsCAgapAgEBBBIMEDEwMDAwMDA1MzAyMjEzMTcwHwICBqgCAQEEFhYUMjAxOS0wNS0yMlQxNjowNzo0MFowHwICBqoCAQEEFhYUMjAxOS0wNS0yMlQxNjowNzo0MFowIQICBqYCAQEEGAwWY29tLnZuZy5kem0ydGgudGhldHVhbjCCAVwCARECAQEEggFSMYIBTjALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI4OTM5MTA0MBsCAgapAgEBBBIMEDEwMDAwMDA1Mjg5MzkxMDQwHwICBqgCAQEEFhYUMjAxOS0wNS0xOFQwOTo0ODoxMlowHwICBqoCAQEEFhYUMjAxOS0wNS0xOFQwOTo0ODoxMlowIgICBqYCAQEEGQwXY29tLnZuZy5kem0ydGgudGhldGhhbmcwggFcAgERAgEBBIIBUjGCAU4wCwICBqwCAQEEAhYAMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQIwDAICBq4CAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwGwICBqcCAQEEEgwQMTAwMDAwMDUyODkzOTI4OTAbAgIGqQIBAQQSDBAxMDAwMDAwNTI4OTM5Mjg5MB8CAgaoAgEBBBYWFDIwMTktMDUtMThUMDk6NDk6MzlaMB8CAgaqAgEBBBYWFDIwMTktMDUtMThUMDk6NDk6MzlaMCICAgamAgEBBBkMF2NvbS52bmcuZHptMnRoLnRoZXRoYW5nMIIBXAIBEQIBAQSCAVIxggFOMAsCAgasAgEBBAIWADALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgECMAwCAgauAgEBBAMCAQAwDAICBq8CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBsCAganAgEBBBIMEDEwMDAwMDA1Mjg5NDgyMDUwGwICBqkCAQEEEgwQMTAwMDAwMDUyODk0ODIwNTAfAgIGqAIBAQQWFhQyMDE5LTA1LTE4VDExOjM0OjMzWjAfAgIGqgIBAQQWFhQyMDE5LTA1LTE4VDExOjM0OjMzWjAiAgIGpgIBAQQZDBdjb20udm5nLmR6bTJ0aC50aGV0aGFuZzCCAVwCARECAQEEggFSMYIBTjALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNTI5Mzg2ODMyMBsCAgapAgEBBBIMEDEwMDAwMDA1MjkzODY4MzIwHwICBqgCAQEEFhYUMjAxOS0wNS0yMFQxNTo0Mjo0OFowHwICBqoCAQEEFhYUMjAxOS0wNS0yMFQxNTo0Mjo0OFowIgICBqYCAQEEGQwXY29tLnZuZy5kem0ydGgudGhldGhhbmcwggFcAgERAgEBBIIBUjGCAU4wCwICBqwCAQEEAhYAMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQIwDAICBq4CAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwGwICBqcCAQEEEgwQMTAwMDAwMDUzMDIyMTkwMjAbAgIGqQIBAQQSDBAxMDAwMDAwNTMwMjIxOTAyMB8CAgaoAgEBBBYWFDIwMTktMDUtMjJUMTY6MDk6MjZaMB8CAgaqAgEBBBYWFDIwMTktMDUtMjJUMTY6MDk6MjZaMCICAgamAgEBBBkMF2NvbS52bmcuZHptMnRoLnRoZXRoYW5noIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb%2FpALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R%2BYFZA1LcLMEsqNSIH3WHhUa%2BiMMTYfSgYMR1TzN5C4spKJfV%2BkhUrhwJzguqS7gpdj9CuTwf0%2Bb8rB9Typj1IawCUKdg7e%2Fpn%2B%2F8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4%2BwHz%2BKO0NP6JlWB7%2BIDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB%2FwQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0%2BiMQzCCX%2BBc0Y2%2BvQ%2B9GvwlktuMhcOAWd%2Fj4tcuBRSsDdu2uP78NS58y60Xa45%2FH%2BR3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z%2BGVIXKDgKDrAEDctuaAEOR9zucgF%2FfLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn%2FG4KT8niBqjSl%2FOnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz%2BI0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22%2FSGAGDDaSK%2BzeGLUR2i0j0I78oGmcFxuegHs5R0UwYS%2FHE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F%2B4fyaRvDRTes58Y4Bhd2RepQcjtjn%2BUC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q%2BhUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq%2FToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo%2FxnfnC6AllLd0KRObn1zeFM78A7SIym5SFd%2FWpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw%2FeVovGJfbs%2BZ3e8bY%2F6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH%2FBAUwAwEB%2FzAfBgNVHSMEGDAWgBQr0GlHlHYJ%2FvRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE%2FP71m%2BLPWybC%2BP7hOHMugFNahui33JaQy52Re8dyzUZ%2BL9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc%2FupGSTKQZEhltV%2FV6WId7aIrkhoxK6%2BJJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT%2BRN5yaZgem8%2BbQp0gF6ZuEujPYhisX4eOGBrr%2FTkJ2prfOv%2FTgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx%2BVnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV%2FX5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw%2BdPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4%2B%2BNwzeajTEV%2BH0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz%2F2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB%2FwQEAwIBBjAPBgNVHRMBAf8EBTADAQH%2FMB0GA1UdDgQWBBQr0GlHlHYJ%2FvRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ%2FvRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR%2BFg4UbmT1HN%2FKpm0COLNSxkBLYvvRzm%2B7SZA%2FLeU802KI%2B%2BXj%2Fa8gH7H05g4tTINM4xLG%2Fmk8Ka%2F8r%2FFmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky%2BERI8GA4nhX1PSZnIIozavcNgs%2Fe66Mv%2BVNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh%2BeeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCadZhuBbSJB5lZTtodmOY1BhPM18XQ1mUZJKj9fsrKXqtLNuxN5CXQSrv2ZFLEFHKIOBWDTl6WBUTJe5k4XaLzhsENKBJcmPAX06%2F2Qrj3isCiD7Io1U%2Ba8%2FRbI%2FQBffTabWjul03sib5JcwsvkY0%2Bd%2FiJ%2B%2Bbu0dD44qjQ827LdPF5phMZG%2BnJpRUK1X2FpH%2Ftd8YhmZOdFYV79HX2fAgkC%2F4y%2B4IIVLQLj%2FBx6QV0lnjJWxpeRYfLO2jhRwg%2BcgDKqnYz%2FUjmEoIWTfvQshG4VATDBRyDjOD89OQGItu22PX1A1gRRXjIWPwKJhATv2NeAivo7gNEVXYGtGiEZOqp&receiptSign=-&roleID=72_90001&serverID=90001&ts=1562121923432&userID=1389007571198853120&sig=37e280d085e2829961258b1c3e907ca0'
        ```
    
        ```json
        {"returnCode":1,"returnMessage":"Success","returnMessage_":"SUCCESS","data":{"orderNumber":1413395937765629952,"orderNumberStr":"1413395937765629952","orderNumberEncoded":"05021-25629-60800-08192","orderStatusMessage":"[MB]SUCCESS","orderDisplayMessage":"[MB]SUCCESS","paymentInstructions":null,"redirectUrl":null,"ztoken":null,"qrCode":null,"balance":null,"smsOtpToken":null,"dbgTransID":null,"dbgStatus":null,"postData":null}}
        ```

<div class="pagebreak"></div>

## Appendix 1. Create signature

* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* Hash key is key_store of `google playstore` & `huawei appgallery`, empty for `apple appstore`

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive.

* All leading and trailing whitespace of a string will have to be trimmed.  

* Example:

    ```bash 
    url = "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item1&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&distSrc=pc&timestamp=1517900546676";
    secret_key = "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    hash_key = "lakdrttwn8yfp"
    raw = "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "lakdrttwn8yfp" + "game" + "item" + "1" + "6789" + "test" + "11517900546676" + "12345"
    sig = md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpclakdrttwn8yfpgameitem16789test1151790054667612345") // "053D245795584604C2AA74F2CA91EF9B"
    ```

## Appendix 2. Quy trình retry Google, Huawei iap transaction

* Payment Support tra cứu GPA trên [Google Play Tool](https://play.google.com/apps/publish/) và lấy thông tin về `purchaseToken`

* Payment Support validate `purchaseToken` qua Google API [Purchases.product:get](https://developers.google.com/android-publisher/api-ref/purchases/products/get) và sử dụng [**developerPayload**](#2-google-developerpayload-entity) để tiến hành giao hàng cho user 

* Payment Support check `inAppPurchaseData` qua Huawei API [purchases/tokens/verify](https://developer.huawei.com/consumer/en/doc/development/HMS-References/iap-api-order-service-purchase-token-verification-v4) và sử dụng [**developerPayload**](#3-huawei-developerpayload-entity) để tiến hành giao hàng cho user 