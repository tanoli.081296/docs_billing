
### **GT Billing**

GT Billing là hệ thống cung cấp thông tin game cho các **Shopfront** để tạo đơn hàng cho **End-user** thực hiện thanh toán và tiến hành giao hàng vào game.

### **Chức năng**

**1. Phục vụ nội bộ**

- Hỗ trợ bán hàng:
    - Web Shop Việt Nam ([https://pay.zing.vn](https://pay.zing.vn)), trang thanh toán game của VNG ở thị trường Việt Nam
    - Web Shop Sea ([https://shop.vng.games](https://shop.vng.games)), trang thanh toán game của VNG ở thị trường SEA`
    - IAP Store: Apple, Google, cung cấp chức năng thực hiện xác thực receipt và giao hàng cho VNG Mobile SDK. [Xem thêm](/GTBilling/iap/)

- Hỗ trợ promotion:
    - Hỗ trợ các chương trình Promotion (của ZaloPay, GT, Game studio) liên quan đến thanh toán của User trên Web Shop. [Xem thêm](/GTBilling/cat1_extapi/)

- Vận hành:
    - Cung cấp tool truy xuất thông tin đơn hàng, cấu hình vật phẩm ...
    - Hỗ trợ công việc đối soát với các hệ thống liên quan

**2. Dịch vụ cho bên ngoài**

- Tích hợp với các shopfront bên ngoài VNG:
    - Unipin Web Topup ([https://unipin.com](https://unipin.com)), trang thanh toán game của Indonesia 

### Tài liệu tích hợp

- [CAT 1 APIs](/GTBilling/cat1_extapi/)
- [IAP Receipt Validation](/GTBilling/iap/)
- [ZMP IAP Receipt Validation](/GTBilling/zmpiap/)
- [Log Format](/GTBilling/log/)

